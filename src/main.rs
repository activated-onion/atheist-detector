// An attempt to simulate the results of this clever psych study:
// https://old.reddit.com/r/atheism/comments/bmdirn/trump_evangelicals_face_a_growing_number_of/
// https://www.rawstory.com/2019/05/trump-evangelicals-face-a-growing-number-of-hidden-atheists/

use rand::prelude::*;

struct Person {
	atheist: bool,
	fluff: [bool; 9],
}

impl Person {
	fn bool_value (b: &bool) -> u32 {
		if *b {
			1
		}
		else {
			0
		}
	}
	
	fn atheist_score (&self) -> u32 {
		Person::bool_value (&self.atheist)
	}
	
	fn control_score (&self) -> u32 {
		self.fluff.iter ().fold (0, |x, b| x + Person::bool_value (b))
	}
	
	fn test_score (&self) -> u32 {
		self.atheist_score () + self.control_score ()
	}
}

fn random_fluff (rng: &mut ThreadRng, fluff_percents: &[usize]) -> [bool; 9] {
	let mut fluff = [false; 9];
	for i in 0..fluff.len () {
		fluff [i] = rng.gen_range (0, 100) < fluff_percents [i];
	}
	fluff
}

fn random_people (rng: &mut ThreadRng, fluff_percents: &[usize], true_atheist_percent: usize, count: usize) -> Vec <Person> 
{
	let mut people = Vec::new ();
	
	let positive_correlation = true;
	let negative_correlation = true;
	
	for i in 0..count {
		// Note that all the atheists are up front - I shuffle them later
		let atheist = i * 100 < true_atheist_percent * count;
		// The fluff questions are totally random at first
		let mut fluff = random_fluff (rng, fluff_percents);
		
		if positive_correlation {
			// However, all atheists are vegetarians!
			if atheist {
				fluff [0] = true;
			}
			
			// Additionally, all theists are meat eaters!
			if ! atheist {
				fluff [0] = false;
			}
		}
		
		if negative_correlation {
			// In fact, no atheist has ever owned a dog.
			if atheist {
				fluff [1] = false;
			}
			
			// And dog ownership is mandatory for theists.
			if ! atheist {
				fluff [1] = true;
			}
		}
		
		people.push (Person {
			atheist: atheist,
			fluff: fluff,
		});
	}
	
	people.shuffle (rng);
	
	people
}

fn generate_people (fluff_percents: &[usize], true_atheist_percent: usize, count: usize) -> Vec <Person>
{
	let mut rng = rand::thread_rng();
	random_people (&mut rng, &fluff_percents, true_atheist_percent, count)
}

fn main() {
	// Configurable variables
	
	let control_size = 10000;
	let test_size = 10000;
	let true_atheist_percent = 15;
	let fluff_percents = [50; 9];
	
	// Configuration ends here
	
	// Generate people
	
	let people = generate_people (&fluff_percents, true_atheist_percent, control_size + test_size);
	
	// Compute things about the generated people
	
	let atheist_count = people.iter ().fold (0u32, |x, p| x + p.atheist_score () as u32);
	let theist_count = people.iter ().fold (0u32, |x, p| x + Person::bool_value (&!p.atheist) as u32);
	let atheist_percent = atheist_count as f32 * 100.0f32 / people.len () as f32;
	
	let control_slice = &people [..control_size];
	let test_slice = &people [control_size..];
	
	let control_average = control_slice.iter ().fold (0, |x, p| x + p.control_score ()) as f32 / control_slice.len () as f32;
	let test_average = test_slice.iter ().fold (0, |x, p| x + p.test_score ()) as f32 / test_slice.len () as f32;
	
	let naive_atheist_estimate = 100.0f32 * (test_average - control_average);
	
	// Print those things
	
	println! ("Generated {} people.", people.len ());
	println! ("{} are atheists ({:.*}%)", atheist_count, 2, atheist_percent);
	println! ("{} are theists", theist_count);
	
	println! ("Average score in control group: {:.*}", 2, control_average);
	println! ("Average score in test group: {:.*}", 2, test_average);
	
	println! ("Naive atheist estimate: {:.*}%", 2, naive_atheist_estimate);
}
